#![feature(iter_array_chunks)]

fn main() {
    let array = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    let chunks = array
        .iter()
        .array_chunks::<3>()
        .collect::<Vec<[&i32; 3]>>();

    println!("Testing of nightly feature `iter_array_chunks`: {chunks:?}");
}
