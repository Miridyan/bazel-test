{ ... }:

let 
  flake = builtins.getFlake (toString ../.);
  nixpkgs = flake.outputs.pkgs.${builtins.currentSystem};
in nixpkgs
