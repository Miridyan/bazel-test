# Bazel Nixpkgs Test


## What Works and Why
The `//tests:hello` target is currently the only thing that works right now.
Everything inside of `//tokio:*` doesn't compile because it depends on crates
retrieved through the `crate_universe` rules. I haven't been able to get those
working in a NixOS environment just yet. It seems that the crate_universe rules
don't piggyback off of the toolchain from the regular rust rules, so it just
tries to download the regular rust compiler (which obviously won't work because
of linker issues on NixOS).


## Is This Actually Necessary

I have no idea, but probably not. There is probably a much easier way to do this
that I don't know about because I was too lazy to research it.


## Taking it for a Spin

Run the following command once you've entered the flake environment.
```
bazel run //tests:hello
```
All the magic to retrieve the rust version comes from `nix/rust.nix` which
uses oxalica's rust overlay to retrieve the nightly compiler. The `nix/default.nix`
file extracts the nixpkgs (plus overlays) from the flake environment and converts
it to a traditional nixpkgs format that can be used as this nixpkgs repository
in other custom nix files consumed by `rules_nixpkgs`.
