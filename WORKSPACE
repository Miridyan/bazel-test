local_repository(
	name = "rules_rust",
	path = "rules/rules_rust",
)

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
http_archive(
    name = "io_tweag_rules_nixpkgs",
    sha256 = "b01f170580f646ee3cde1ea4c117d00e561afaf3c59eda604cf09194a824ff10",
    strip_prefix = "rules_nixpkgs-0.9.0",
    urls = ["https://github.com/tweag/rules_nixpkgs/archive/v0.9.0.tar.gz"],
)

load("@io_tweag_rules_nixpkgs//nixpkgs:repositories.bzl", "rules_nixpkgs_dependencies")
rules_nixpkgs_dependencies()

load("@io_tweag_rules_nixpkgs//nixpkgs:nixpkgs.bzl", "nixpkgs_git_repository", "nixpkgs_local_repository")
nixpkgs_local_repository(
    name = "nixpkgs",
    nix_file = "//nix:default.nix",
    nix_file_deps = [
        "//:flake.nix",
        "//:flake.lock",
    ],
)

load("@io_tweag_rules_nixpkgs//nixpkgs:toolchains/rust.bzl", "nixpkgs_rust_configure")
nixpkgs_rust_configure(
    name = "nix_rust",
    nix_file = "//nix:rust.nix",
    repository = "@nixpkgs",
)

load("@rules_rust//rust:repositories.bzl", "rust_repositories")
rust_repositories(edition = "2021")

load("@rules_rust//crate_universe:repositories.bzl", "crate_universe_dependencies")
crate_universe_dependencies(
    bootstrap = True,

    rust_toolchain_cargo_template = "@nix_rust//:bin/cargo",
    rust_toolchain_rustc_template = "@nix_rust//:bin/rustc",
)

load("@rules_rust//crate_universe:defs.bzl", "crate", "crates_repository", "render_config")
crates_repository(
    name = "crate_index",
    cargo_lockfile = "//:Cargo.Bazel.lock",
    lockfile = "//:cargo-bazel-lock.json",
    generator = "@cargo_bazel_bootstrap//:cargo-bazel",

    rust_toolchain_cargo_template = "@nix_rust//:bin/cargo",
    rust_toolchain_rustc_template = "@nix_rust//:bin/rustc",

    supported_platform_triples = [
        "x86_64-unknown-linux-gnu",
    ],

    packages = {
        "bytes": crate.spec(
            version = "1.2.1",
        ),
        "tokio": crate.spec(
            version = "1.21.2",
            features = ["full"],
        ),
        "tokio-util": crate.spec(
            version = "0.7.4",
            features = ["full"],
        ),
        "futures": crate.spec(
            version = "0.3.25",
            features = ["thread-pool"],
        ),
    },
)

load("@crate_index//:defs.bzl", "crate_repositories")
crate_repositories()
