# So bazel downloads a version of rustc compiled on another linux distro
# where /lib64/ld-linux-x86-64.so.2 is hardcoded into the binary. Patching
# is it out of the question, so I need to come back and make sure that I
# enable the `nix-ld` feature for nixos so that I can use it here to run
# downloaded binaries.
{
  description = "Test of Bazel build system within nix";

  inputs = {
    nixpkgs.url      = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url  = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
  };

  outputs = { self, nixpkgs, rust-overlay, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
      in
      with pkgs;
      {
        inherit pkgs;

        devShells.default = mkShell {
          buildInputs = [
            bazel_5
            bazel-buildtools
            # rust-analyzer
          ];
        };
      }
    );
}
